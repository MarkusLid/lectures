```python tags=["initialize"]
from matplotlib import pyplot as plt 
import numpy as np
from math import pi
```
# Solutions for lecture 14 exercises

## Exercise 1: Crossover between extrinsic and intrinsic regimes 

### Subquestion 1
Law of mass action:
$$ n_e n_h = \frac{1}{2} \left(\frac{k_BT}{\pi\hbar^2}\right)^3 
(m_e^{\ast}m_h^{\ast})^{3/2}e^{-\beta E_{gap}}$$

Charge balance condition:
$$ n_e - n_h + n_D - n_A = N_D - N_A $$
### Subquestion 2
 
$$ n_{e} = \frac{1}{2}(\sqrt{D^2+4n_i^2}+D)$$
$$ n_{h} = \frac{1}{2}(\sqrt{D^2+4n_i^2}-D)$$
where $D = N_D - N_A$ and $n_i=n_{e,intrinsic}=n_{h,intrinsic}$.

### Subquestion 3

If $D<<n_i$, then the doping is not important and results of intrinsic are
reproduced.
Contrarily, if $D>>n_i$, it's mostly the doping that determines $n_e$ and $n_h$.
The thermal factor becomes unimportant.
Check both cases with lecture notes approximated solutions by doing a Taylor expansion. 


## Exercise 2: Donor ionization

### Subquestion 1

If all the dopants are ionized, the Fermi level gets shifted up towards the conduction band.
This result can be obtained when using results in Exercise 1 - Subquestion 2 and the following:

$$ n_e - n_h = N_D - N_A $$


### Subquestion 2
Now,
$$ n_D = N_D\frac{1}{e^({E_D-E_F})/k_BT+1} $$
### Subquestion 3

??? hint "how?"
    Use Germianium Fermi Energy at room temperature and solve E_F 
    via using the n_e solution in Exercise 1 and by applying the definition of n_e.
    Check [key algorithm of describing the state of a semiconductor](/13_semiconductors/#part-1-pristine-semiconductor)

## Exercise 3: Performance of a diode

### Subquestion 1

Intrinsic semiconductors have no impurities. Adding dopant atoms creates extra unbounded
electrons/holes depending on the n/p dopant atom added. Impurity eigenstates appear and 
the $E_F$ level shifts (up/down for added donors/acceptors).

To make a diode a p-n junction is needed (extrinsic semiconductors). Drawing a diagram is
very helpful.

### Subquestion 2

Under reverse bias only two processes carry out current: electrons that may be thermally
excited into the conduction band (p-doped side) and holes that may be thermally
excited into the valence band (n-doped side).

### Subquestion 3

$$ I_s(T) \propto e^{-E_{gap}/k_BT}$$




## Exercise 4: Quantum well heterojunction in detail

### Subquestion 1

![](figures/diagram_14.png)


### Subquestion 2
This a "particle in a box" problem.
$$-\frac{\hbar^2}{2m_e^{\ast}} \frac{\partial^2 \Psi_e(z)}{\partial z^2} = (E_e-E_c)\Psi_e $$
$$-\frac{\hbar^2}{2m_h^{\ast}} \frac{\partial^2 \Psi_h(z)}{\partial z^2} = (E_v-E_h)\Psi_h $$


### Subquestion 3

$$E_e = E_c + \frac{\hbar^2 (k_x^2+k_y^2)}{2m_e^{\ast}}$$
$$E_h = E_v - \frac{\hbar^2 (k_x^2+k_y^2)}{2m_h^{\ast}}$$

### Subquestion 4
This is a 2D electron/hole gas. Apply 2D density of states (constant).
$$g_e = \frac{4 \pi m_e^{\ast}}{\hbar^2}$$
$$g_h = \frac{4 \pi m_h^{\ast}}{\hbar^2}$$

### Subquestion 5
L can be found here for $k_x$ and $k_y$ using previous subquestions.
Setting $$ E_e - E_h - E_c + E_v = 1 eV = \frac{\hbar^2 (k_x^2+k_y^2)}{2}
(\frac{1}{m_e^{\ast}}+\frac{1}{m_h^{\ast}})$$

### Subquestion 6

For a laser one wants to fix the emission wavelength to a certain value. With
this setup the band gap is "easy" to design (set by L, which is fixed).

### Subquestion 7
If donor impurities are put outside of the well (on both sides, for example)
the donated electrons can reduce their energies by falling into the well,
but the ionized dopants remain behind. This gives an advanttage because an
extremely high mobility for electrons can be obtained within the quantum well
(there are no ionized dopants in the well to scatter off of).
This is called modulation doping.


