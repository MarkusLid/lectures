```python tags=["initialize"]
 
from matplotlib import pyplot

import numpy as np
from scipy.optimize import curve_fit
from scipy.integrate import quad

from common import draw_classic_axes, configure_plotting

configure_plotting()

def sqrt_plus(x):
    return np.sqrt(x * (x >= 0))

# Band structure parameters.
E_V, E_C, E_F = -1.2, 1.8, .4
E_D, E_A = E_C - .7, E_V + .5
m_h, m_e = 1, .5
```

# Semiconductor physics
_(based on chapters 17–18 of the book)_  


!!! success "Expected prior knowledge"

    Before the start of this lecture, you should be able to:

    - Simplify integral expressions by Taylor expansion
    - Compute the density of states of the free electron model
    - Apply the concepts of group velocity and effective mass to solve problems

!!! summary "Learning goals"

    After this lecture you will be able to:

    - Describe the concept of holes, and apply this concept to describe the properties of semiconductors
    - Compute the density of states of electrons in semiconductors
    - Compute the density of charge carriers and chemical potential as a function of temperature

??? info "Lecture video"

    <iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/IxDp_JAtBQs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Review of band structure properties

* Group velocity $v=\hbar^{-1}\partial E(k)/\partial k$
* Effective mass $m_{eff} = \hbar^2\left(d^2 E(k)/dk^2\right)^{-1}$
* Density of states $g(E) = \sum_{\textrm{FS}} (dn/dk) \times (dk/dE)$ the amount of states per infinitesimal interval of energy at each energy.

A simple check that everything is correct is the free electron model:

$$H = \hbar^2 k^2/2m$$

The velocity is $\hbar^{-1}\partial E(k)/\partial k = \hbar k / m \equiv p/m$.  
The effective mass is $m_{eff} = \hbar^2\left(d^2 E(k)/dk^2\right)^{-1} = m$.

So in the simplest case the definitions match the usual expressions.

#### Why are these properties important?

* Group velocity descibes how quickly electrons interacting with the lattice move.
* Effective mass tells us how hard it is to *accelerate* the particles and it enters $g(E)$ for a parabolic band
* Density of states is required to determine the Fermi level, heat capacity, etc.

## Filled vs empty bands

A completely filled band is very similar to a completely empty band.

In a filled band $n(E)=1$ because $|E - E_F| \gg kT$. In an empty band $n(E)=0$.

Heat capacity $C_v = \tfrac{d}{dT}\int_{-\infty}^\infty E\times g(E) \times dE\times n_F(E, T) = 0$.

A completely filled band carries no electric current:
$$
\begin{align}
j = 2e \frac{1}{2\pi} \int_{-\pi/a}^{\pi/a} v(k) dk = 2e \frac{1}{2\pi \hbar} \int_{-\pi/a}^{\pi/a} \frac{dE}{dk} \times dk = \\
2e \frac{1}{2\pi \hbar} [E(-\pi/a) - E(\pi/a)] = 0
\end{align}
$$

## From electrons to holes

A completely filled band $\approx$ completely empty band. The idea of introducing **holes** is to transform an *almost* completely filled band $\Rightarrow$ almost empty one. Instead of describing a lot of electrons that are present, we focus on those that are absent.

Definition: a **hole** is a state of a completely filled band with one particle missing.

![](figures/holes.svg)

In this schematic we can either say that 8×2 electron states are occupied (the system has 8×2 electrons counting spin), or 10×2 hole states are occupied. A useful analogy to remember: glass half-full or glass half-empty.

## Properties of holes

The probability for an electron state to be occupied in equilibrium is $f(E)$:

$$f(E) = \frac{1}{e^{(E-E_F)/kT} + 1}$$

The probability for a hole state to be occupied:

$$1 - f(E) = 1 - \frac{1}{e^{(E-E_F)/kT} + 1} = \frac{1}{e^{(-E+E_F)/kT} + 1}$$

therefore for holes both energy $E_h= -E$ and $E_{F,h} = -E_F$.

The **momentum** $p_h$ of a hole should give the correct total momentum of a partially filled band if one sums momenta of all holes. Therefore $p_h = -\hbar k$, where $k$ is the wave vector of the electron.

Similarly, the total **charge** should be the same regardless of whether we count electrons or holes, so holes have a positive charge $+e$ (electrons have $-e$).

On the other hand, the velocity of a hole is **the same**:
$$\frac{dE_h}{dp_h} = \frac{-dE}{-d\hbar k} = \frac{dE}{dp}$$

Finally, we derive the hole effective mass from the equations of motion:

$$m_h \frac{d v}{d t} = +e (E + v\times B)$$

Comparing with

$$m_e \frac{d v}{d t} = -e (E + v\times B)$$

we get $m_h = -m_e$.

## Semiconductors: materials with two bands.

In semiconductors the Fermi level is between two bands. The unoccupied band is the **conduction band**, the occupied one is the **valence band**. In the conduction band the **charge carriers** (particles carrying electric current) are electrons, in the valence band they are holes.

We can control the position of the Fermi level (or create additional excitations) making semiconductors conduct when needed.

Only the bottom of the conduction band has electrons and the top of the valence band has holes because the temperature is always smaller than the size of the band gap.

Therefore we can approximate the dispersion relation of both bands as parabolic.

![](figures/semiconductor.svg)

Or in other words

$$E_e = E_G + \frac{\hbar^2k^2}{2m_e},$$
$$E_h = \frac{\hbar^2k^2}{2m_h}.$$

Here $E_G$ is the band gap, and the top of the valence band is at $E=0$.

Observe that because we are describing particles in the valence band as holes, $m_h > 0$ and $E_h > 0$.

??? question "a photon gives a single electron enough energy to move from the valence band to the conduction band. How many particles does this process create?"
    Two: one electron and one hole.

## Semiconductor density of states and Fermi level

### Part 1: pristine semiconductor

```python
E = np.linspace(-3, 3, 1000)
fig, ax = pyplot.subplots()

n_F = 1/(np.exp(2*(E - E_F)) + 1)
g_e = m_e * sqrt_plus(E - E_C)
g_h = m_h * sqrt_plus(E_V - E)
ax.plot(E, g_e, label="$g_e$")
ax.plot(E, g_h, label="$g_h$")
ax.plot(E, 10 * g_h * (1-n_F), label="$n_h$")
ax.plot(E, 10 * g_e * n_F, label="$n_e$")
ax.plot(E, n_F, label="$n_F$", linestyle='dashed')
ax.set_ylim(top=1.5)

ax.set_xlabel('$E$')
ax.set_ylabel('$g$')
ax.set_xticks([E_V, E_C, E_F])
ax.set_xticklabels(['$E_V$', '$E_C$', '$E_F$'])
ax.legend()
draw_classic_axes(ax, xlabeloffset=.2)
```

Electrons
$$ E = E_G + {p^2}/{2m_e}$$

$$ g(E) = (2m_e)^{3/2}\sqrt{E-E_G}/2\pi^2\hbar^3$$

Holes
$$ E_h = {p^2}/{2m_h}$$

$$ g(E_h) = (2m_h)^{3/2}\sqrt{E_h}/2\pi^2\hbar^3$$

**The key algorithm of describing the state of a semiconductor:**

1. Compute the density of states of all types of particles.
2. Calculate the total amount of electrons and holes, assuming a certain value of $E_F$
3. Write down the charge balance condition: the difference between electrons and holes should equal to the total charge of the semiconductor.
4. Apply approximations to simplify the equations (this is important!).
5. Find $E_F$ and concentrations of electrons and holes

Applying the algorithm:

$$n_h = \int_0^\infty f(E+E_F)g_h(E)dE = \int_0^\infty\frac{(2m_h)^{3/2}}{2\pi^2\hbar^3} \sqrt{E}\frac{1}{e^{(E+E_F)/kT}+1}dE$$

$$n_e = \int_{E_G}^\infty f(E-E_F)g_e(E)dE = \int_{E_G}^\infty\frac{(2m_e)^{3/2}}{2\pi^2\hbar^3} \sqrt{E-E_G}\frac{1}{e^{(E-E_F)/kT}+1}dE$$

We need to solve $n_e = n_h$

Simplification:
Fermi level is far from both bands $E_F \gg kT$ and $E_G - E_F \gg kT$

Therefore Fermi-Dirac distribution is approximately similar to Boltzmann distribution.

$$f(E\pm E_F) \approx e^{-(E\pm E_F)/kT}$$

Now we can calculate $n_e$ and $n_h$:

$$n_h \approx \frac{(2m_h)^{3/2}}{2\pi^2\hbar^3}e^{-E_F/kT} \int_0^\infty\sqrt{E}e^{-E/kT}dE =
N_V e^{-E_F/kT},$$

with

$$N_V = 2\left(\frac{2\pi m_h kT}{h^2}\right)^{3/2}$$

the density of holes with energy $E<kT$ (compare with the rule above).

??? question "how large is $N_V$ at room temperature? (hard question)"
    If $kT \sim 1\textrm{eV}$ (the typical energy size of a band), then electrons in the whole band may be excited and $N_V \sim 1$ per unit cell. On the other hand, $N_V \sim T^{3/2}$ Therefore $N_V \sim (kT/1 \textrm{eV})^{3/2}\sim 1\%$.

Similarly for electrons:

$$n_e = N_C e^{-(E_G - E_F)/kT},\quad N_C = 2\left(\frac{2\pi m_e kT}{h^2}\right)^{3/2}$$


Combining everything together:

$$n_h \approx N_V e^{-E_F/kT} = N_C e^{-(E_G-E_F)/kT} \approx n_e$$

Solving for $E_F$:

$$E_F = \frac{E_G}{2} - \frac{3}{4}kT\log(m_e/m_h)$$

An extra observation: regardless of where $E_F$ is located, $n_e n_h = N_C N_V e^{-E_G/kT} \equiv n_i^2$.

$n_i$ is the **intrinsic carrier concentration**, and for a pristine semiconductor $n_e = n_h = n_i$.

> The equation
> $$n_e n_h = n_i^2$$
> is the **law of mass action**. The name is borrowed from chemistry, and describes the equilibrium concentration of two reagents in a reaction $A+B \leftrightarrow AB$. Here electrons and hole constantly split and recombine.


## Exercises

#### Exercise 1: Energy, mass, velocity and cyclotron motion of electrons and holes

  1. Consider the top of the valence band of a semiconductor (see [above](#semiconductors-materials-with-two-bands)). Does an electron near the top of the valence band have a positive or a negative effective mass? Does the electron's energy increase or decrease as $k$ increases from 0? Does the electron have a positive or negative group velocity for $k>0$?
  2. Answer the same last 3 questions for a hole in the valence band.
  3. We now consider an electron in a 2D semiconductor near the bottom of the conduction band described by an energy dispersion $E=E_{G}+\frac{\hbar^2}{2m_{eff}}(k_x^2+k_y^2)$. The electron's velocity is given by $\mathbf{v}=\nabla_\mathbf{k} E/\hbar = \frac{1}{\hbar}(\frac{\partial E}{\partial k_x}\mathbf{\hat{x}} + \frac{\partial E}{\partial k_y}\mathbf{\hat{y}})$. Suppose we turn on a magnetic field $B$ in the $z$-direction. Write down the equation of motion for this electron (neglecting collisions). What is the shape of the motion of the electron? What is the characteristic 'cyclotron' frequency of this motion? What is the direction of the Lorentz force with respect to $\nabla_\mathbf{k} E$? 
  4. Suppose we now consider a hole near the bottom of the conduction band and turn on a magnetic field $B$ in the $z$-direction. Is the direction of the circular motion (i.e., the chirality) of the hole the same as that of the electron? Would the chirality change if we instead consider a hole (or electron) near the top of the valence band?

#### Exercise 2: holes in Drude and tight binding model

1. Recall from the [Drude model](/3_drude_model/) that electrons give rise to a negative Hall coefficient. Explain why the Hall coefficient is positive if holes are the charge carriers in a material.
2. What would be the Hall coefficient if both carriers with equal concentration are present? Assume that both electrons and holes can move freely and have the same scattering time.

    Recall that the dispersion relation of a [1D single orbital tight binding chain](/7_tight_binding/) is given by
    $$E(k)=\varepsilon + 2t \cos(ka),$$
    where $a$ is the lattice constant and $\varepsilon$ and $t$ are tight binding parameters.

3. What is the group velocity and effective mass of this band for holes compared to that of electrons?
4. Give an integral expression of the hole concentration in this band given the chemical potential $\mu$ and temperature $T$.
5. Show that the sum of the electron and hole concentration in this band is constant as a function of the temperature.

#### Exercise 3: a 1D semiconductor
Suppose we have a 1D semiconductor with a conduction band described by $$E_{cb} = E_G - 2 t_{cb} [\cos(ka)-1],$$ and a valence band described by $$E_{vb} = 2 t_{vb} [\cos(ka)-1].$$ Furthermore, the chemical potential is set at $0 < \mu < E_G$.

1. Derive an expression for the group velocity and effective mass for electrons in the conduction bands and holes in the valence band.

    Assume that the Fermi level is far away from both bands. That is, $|E - \mu| \gg k_B T$. In that case, it is acceptable to approximate the bands for low $k$.

2. Why is it acceptable? Write down an approximate expression of these bands.
3. Write down an expression for the density of states _per unit length_ for both bands using the approximated expressions. Compare with the actual density of states per unit length. 
4. Calculate the electron density in the conduction band and the hole density in the valence band.
5. What would the chemical potential $\mu$ be in case of an intrinsic semiconductor?
