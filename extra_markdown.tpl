{% extends 'markdown.tpl' %}

{% block input %}
{% if "initialize" in cell.metadata.tags %}
<div class="thebelab-init-code">
  <pre class="thebelab-code" data-executable="true" data-language="python">
    {{cell.source}}
  </pre>
</div>
{% else %}
<pre class="thebelab-code" data-executable="true" data-language="python">{{cell.source}}</pre>
{% endif %}
{% endblock %}

{% block display_data %}
{%- if output.data['text/html'] -%}
<div class="thebelab-output" data-output="true">{{ output.data['text/html'] }}{% else %}
<div class="thebelab-output" data-output="true" markdown="1">
{{ super() }}
{%- endif -%}
</div>
{% endblock %}

{% block error %}
{% endblock error %}

{% block stream %}
{%- if output.name == 'stdout' -%}
{{ output.text | indent }}
{%- endif -%}
{% endblock stream %}
